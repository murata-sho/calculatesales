package jp.alhinc.mizoguchi_tetsuya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		Map<String, String> map = new HashMap<String, String>();
		Map<String, Long> map2 = new HashMap<String, Long>();

		// 支店定義ファイル読み込み

		try {

			BufferedReader br = null;

			File file = new File(args[0], "branch.lst");
			if (!(file.exists())) {
				System.out.print("支店定義ファイルが存在しません");
			}

			try {
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;
				while ((line = br.readLine()) != null) {
					System.out.println(line.split(",")[0].matches("\\d{3}"));

					String[] code = line.split(",", -1);
					map.put(code[0], code[1]);
					map2.put(code[0], 0L);

					if (!(code[0].matches("\\d{3}")) || code.length != 2) {
						System.out.print("支店定義ファイルのフォーマットが不正です");
						return;
					}

				}

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
					}
				}

			}

			// 集計

			File dir = new File(args[0]);

			File[] filename = dir.listFiles();

			for (int i = 0; i < filename.length; ++i) {
				System.out.println("file name : " + filename[i]);

			}

			FilenameFilter filter = new FilenameFilter() {

				public boolean accept(File file, String str) {

					if (str.matches("\\d{8}\\.rcd")) {
						return true;
					} else {
						return false;
					}

				}
			};

			File[] fileSerial = new File(args[0]).listFiles(filter);
			Arrays.sort(fileSerial);
			for (int i = 0; i < fileSerial.length - 1; ++i) {
				String a = fileSerial[i].getName();
				String b = fileSerial[i + 1].getName();
				String[] c = a.split("\\.");
				String[] d = b.split("\\.");

				int cc = Integer.parseInt(c[0]);
				int dd = Integer.parseInt(d[0]);

				if (dd - cc != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}

			}

			BufferedReader bufferedReader = null;

			try {

				File[] file2 = new File(args[0]).listFiles(filter);

				for (int i = 0; i < file2.length; i++) {

					FileReader fileReader = new FileReader(file2[i]);
					bufferedReader = new BufferedReader(fileReader);

					String data = bufferedReader.readLine();
					String data2 = bufferedReader.readLine();
					String data4 = bufferedReader.readLine();

					if (data4 != null) {
						System.out.print(file2[i] + "のフォーマットが不正です");
						fileReader.close();
						return;
					}

					long data3 = Long.parseLong(data2);

					if (!(map.containsKey(data))) {
						System.out.println(file2[i] + "の支店コードが不正です");
						fileReader.close();
						return;
					}
					map2.put(data, map2.get(data) + data3);

					System.out.println(map2.get(data));

					String num = Long.toString(Math.abs(map2.get(data)));
					if (num.length() > 10) {
						System.out.println("桁数が10桁を超えました");
						fileReader.close();
						return;
					}
					fileReader.close();

				}

			} catch (IOException e) {
				e.printStackTrace();

			} finally {
				bufferedReader.close();
			}
		} catch (

		IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}
		BufferedWriter bw = null;

		// 集計結果出力

		try {
			File file3 = new File(args[0], "branch.out");
			FileWriter fw = new FileWriter(file3);
			bw = new BufferedWriter(fw);

			for (Map.Entry<String, String> entry : map.entrySet()) {

				System.out.println(entry.getKey() + "," + entry.getValue() + "," + map2.get(entry.getKey()));
				bw.write(entry.getKey() + "," + entry.getValue() + "," + map2.get(entry.getKey()));

				bw.newLine();
			}
			bw.close();

		} catch (IOException e) {
			System.out.println(e);
		}
	}
}